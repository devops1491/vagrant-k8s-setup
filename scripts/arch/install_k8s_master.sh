#!/bin/bash
set -e

# Install etcd database
# yay -Sa --noconfirm etcd

# Install K8s control-plane
sudo pacman -Rdd --noconfirm iptables \
&& sudo pacman -S --noconfirm kubernetes-control-plane \
&& sudo pacman -S --noconfirm kubeadm kubectl cni-plugins cri-o

# Disable swap
sudo swapoff -a \
&& sudo sed -i '/swap/ s/^/#/' /etc/fstab

# Change kubelet default cgroup_manager
sudo sed -i "/KUBELET_ARGS/ s/$/ --cgroup-driver='systemd'/" /etc/kubernetes/kubelet.env

# Enable & start CRI
sudo systemctl enable --now crio

# Enable & start kubelet
sudo systemctl enable --now kubelet

# Create the cluster with kubeadm
sudo kubeadm init --pod-network-cidr='10.85.0.0/16' --cri-socket='unix:///run/crio/crio.sock'

# Configure kubectl
mkdir -p $HOME/.kube \
&& sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config \
&& sudo chown $(id -u):$(id -g) $HOME/.kube/config
