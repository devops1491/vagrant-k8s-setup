#!/bin/bash
set -e

# Custom settings
echo "alias ll='ls -l --color'" >> /home/vagrant/.bashrc
echo "alias la='ls -la --color'" >> /home/vagrant/.bashrc

# Download packages dataabse
sudo pacman -Sy

# Install AUR
sudo pacman -S --noconfirm --needed base-devel git \
&& git clone https://aur.archlinux.org/yay.git \
&& cd yay \
&& makepkg -sic --noconfirm
